# Introduction to Ansible

This repository includes slides for an Introduction to Ansible course. Please
see the [course outline](slides/course-outline.md) for more information.

## Slides

The slides are setup using [reveal.js](https://github.com/hakimel/reveal.js/)
and can be viewed using [Docker](https://www.docker.com/) as shown below.

```
# Build the container
docker build -t ansible-intro-slides:latest .

# Run the docker container and view slides at http://localhost:8000
docker run -p 8000:8000 -d --name intro-slides ansible-intro-slides:latest

# Clean up when you're done
docker rm -f intro-slides
docker rmi ansible-intro-slides:latest
```
