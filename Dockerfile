FROM node:11

WORKDIR /usr/src/slides

COPY slides .

RUN npm install && npm audit fix

EXPOSE 8000
CMD ["npm", "start"]
