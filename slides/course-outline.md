### Course outline

* [Introduction to Ansible](#/)
  * [Background](#/4)
  * [Types of Automation](#/5)
* [Ansible Background](#/6)
* [Installing Ansible](#/7)
* [Terminology](#/8)
* [Ad-Hoc Commands](#/9)


* [Configuration Management](#/10)
* [Variables](#/11)
* [Ansible Templates](#/12)
* [Iteration and Conditionals](#/13)
* [Ansible Vault](#/14)
* [Filters](#/15)


* [Lookup Plugins](#/16)
* [Handlers](#/17)
* [Roles](#/18)
* [Wrap Up](#/19)
